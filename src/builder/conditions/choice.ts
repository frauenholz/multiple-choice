/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    arraySize,
    collection,
    detached,
    editor,
    isFilledString,
    markdownifyToString,
    pgettext,
    tripetto,
} from "tripetto";
import { MultipleChoice } from "..";
import { Choice } from "../choice";

/** Assets */
import ICON from "../../../assets/condition.svg";
import ICON_SINGLE from "../../../assets/single.svg";
import ICON_MULTIPLE from "../../../assets/multiple.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    alias: "multiple-choice",
    icon: ICON,
    get label() {
        return pgettext("block:multiple-choice", "Choice");
    },
})
export class MultipleChoiceCondition extends ConditionBlock {
    @affects("#condition")
    @collection("#choices")
    choice: Choice | undefined;

    get icon() {
        return this.node?.block instanceof MultipleChoice &&
            this.node?.block.multiple
            ? ICON_MULTIPLE
            : ICON_SINGLE;
    }

    get name() {
        return this.choice?.name || this.type.label;
    }

    get label() {
        return this.node?.block instanceof MultipleChoice
            ? this.node.block.alias || this.node.label || ""
            : "";
    }

    get choices() {
        return (
            (this.node &&
                this.node.block instanceof MultipleChoice &&
                this.node.block.choices) ||
            undefined
        );
    }

    @detached("slot")
    migrate(): (() => void) | void {
        if (this.choice && this.node?.block instanceof MultipleChoice) {
            const slots = this.node.block.slots;
            const ref = this.node.block.multiple ? this.choice.id : "choice";

            return () => {
                if (this.node?.block instanceof MultipleChoice) {
                    this.slot = slots.select(ref);

                    return true;
                }

                return false;
            };
        }
    }

    @editor
    defineEditor(): void {
        if (this.node && this.choices) {
            const choices: Forms.IDropdownOption<Choice>[] = [];

            this.choices.each((choice) => {
                if (isFilledString(choice.name)) {
                    choices.push({
                        label: markdownifyToString(choice.name),
                        value: choice,
                    });
                }
            });

            if (arraySize(choices) > 0) {
                this.editor.form({
                    title: this.node.label,
                    controls: [
                        new Forms.Dropdown<Choice>(
                            choices,
                            Forms.Dropdown.bind(this, "choice", undefined)
                        ),
                    ],
                });
            }
        }
    }
}
