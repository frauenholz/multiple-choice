/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";

/** Assets */
import ICON from "../../../assets/undefined.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:undefined`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:multiple-choice", "No choice made");
    },
})
export class MultipleChoiceUndefinedCondition extends ConditionBlock {}
